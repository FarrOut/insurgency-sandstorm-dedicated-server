###########################################################
# Dockerfile that builds an Insurgency Sandstorm Gameserver
###########################################################
FROM cm2network/steamcmd:root

LABEL maintainer="gallopingsparrow@gmail.com"

ENV STEAMAPPID 581330
ENV STEAMAPPDIR /home/steam/insurgency-sandstorm-dedicated

COPY entry.sh ${STEAMAPPDIR}/entry.sh
RUN chmod 755 ${STEAMAPPDIR}/entry.sh

ENV SRCDS_FPSMAX=300 \
	SRCDS_TICKRATE=128 \
	SRCDS_PORT=27015 \
	SRCDS_TV_PORT=27020 \
	SRCDS_CLIENT_PORT=27005 \
	SRCDS_MAXPLAYERS=14 \
	SRCDS_TOKEN=0 \
	SRCDS_RCONPW="asdasd21ffsdf" \
	SRCDS_PW="asdfasdfasdfvvvvv" \
	SRCDS_STARTMAP="de_dust2" \
	SRCDS_REGION=3 \
	SRCDS_MAPGROUP="mg_active" \
	SRCDS_GAMETYPE=0 \
	SRCDS_GAMEMODE=1

USER steam

WORKDIR $STEAMAPPDIR

VOLUME $STEAMAPPDIR
VOLUME Insurgency/Saved/Config/LinuxServer

RUN ${STEAMCMDDIR}/steamcmd.sh +login anonymous +force_install_dir ${STEAMAPPDIR} +app_update ${STEAMAPPID} +quit
ENTRYPOINT ${STEAMAPPDIR}/srcds_run -game InsurgencyServer -console -port $SRCDS_PORT

# Expose ports
EXPOSE 27015/tcp 27015/udp 27020/udp 27036/tcp 27036/udp
